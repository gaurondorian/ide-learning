import Image from "next/image";
import Book from '../../public/book.svg';
import {useState} from "react";

export default function SideBar(){
    const nbCourse = 6;
    const [curCourse,setCurCourse] = useState(5);
    function handleChange(e:any) {
        setCurCourse(e.target.id);
    }

    return(

        <aside className="bg-white h-screen w-1/5 text-[#2405F2] font-medium">
            <div className="flex flex-col align-items justify-center">
                <div className="flex flex-col mb-8">
                    <h1 className="font-medium py-8 pl-8">Nom du cours</h1>
                    <div className="mb-1 text-base font-medium text-gray-600 ml-[75%]">{Math.round(100*(curCourse/nbCourse))}%</div>
                    <div className="w-4/5 bg-gray-200 rounded-full h-2.5 mb-4 ml-8">
                        <div className="bg-gray-600 h-2.5 rounded-full" style={{width:Math.round(100*(curCourse/nbCourse)).toString()+"%"}}></div>
                    </div>
                    <div className=" text-gray-400 text-xs ml-[10%]">Partie {curCourse} sur {nbCourse}</div>
                </div>
                <ul className="place-self-center pr-1">
                    <li className="pb-8">Chapitre - 1
                        <ul className="ml-8 text-sm font-light text-gray-900">
                            <li onClick={handleChange}  id="1" className={curCourse==1?"font-medium flex flex-row pt-4":"flex flex-row pt-4 hover:cursor-pointer"}><Image src={Book} alt="book icon" className="mr-2" />Section - 1 {curCourse==1?<p className="ml-2 text-xs place-self-center">(Modification)</p>:null}</li>
                        </ul>
                    </li>
                    <li className="pb-8">Chapitre - 2
                        <ul className="ml-8 text-sm font-light text-gray-900">
                            <li onClick={handleChange} id="2" className={curCourse==2?"font-medium flex flex-row pt-4":"flex flex-row pt-4 hover:cursor-pointer"}><Image src={Book} alt="book icon" className="mr-2" />Section - 2 {curCourse==2?<p className="ml-2 text-xs place-self-center">(Modification)</p>:null}</li>
                            <li onClick={handleChange}  id="3" className={curCourse==3?"font-medium flex flex-row pt-4":"flex flex-row pt-4 hover:cursor-pointer"}><Image src={Book} alt="book icon" className="mr-2" />Section - 3 {curCourse==3?<p className="ml-2 text-xs place-self-center">(Modification)</p>:null}</li>
                            <li onClick={handleChange}  id="4" className={curCourse==4?"font-medium flex flex-row pt-4":"flex flex-row pt-4 hover:cursor-pointer"}><Image src={Book} alt="book icon" className="mr-2" />Section - 4 {curCourse==4?<p className="ml-2 text-xs place-self-center">(Modification)</p>:null}</li>
                        </ul>
                    </li>
                    <li className="pb-8">Chapitre - 3
                        <ul className="ml-8 text-sm font-light text-gray-900">
                            <li onClick={handleChange}  id="5" className={curCourse==5?"font-medium flex flex-row pt-4":"flex flex-row pt-4 hover:cursor-pointer"}><Image src={Book} alt="book icon" className="mr-2" />Section - 5 {curCourse==5?<p className="ml-2 text-xs place-self-center">(Modification)</p>:null}</li>
                            <li onClick={handleChange}  id="6" className={curCourse==6?"font-medium flex flex-row pt-4":"flex flex-row pt-4 hover:cursor-pointer"}><Image src={Book} alt="book icon" className="mr-2" />Section - 6 {curCourse==6?<p className="ml-2 text-xs place-self-center">(Modification)</p>:null}</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>
    )
}