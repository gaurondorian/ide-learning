import ProfileCard from "@/components/ProfileCard";
import Link from "next/link";

export default function MyHeader() {
  return (
    <nav className="bg-primary border-gray-200">
      <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <Link href="/">
          <span className="self-center text-2xl font-semibold whitespace-nowrap text-white">
            [IDE - Le@rn!ing]
          </span>
        </Link>
        <ProfileCard name="Léo" nbAnnee={2} />
      </div>
    </nav>
  );
}
