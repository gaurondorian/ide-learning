"use client";

import * as React from "react";
import Image from "next/image";
import { DropdownMenuCheckboxItemProps } from "@radix-ui/react-dropdown-menu";

import { Button } from "@/components/ui/button";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
  DropdownMenuRadioGroup,
  DropdownMenuRadioItem,
  DropdownMenuSub,
  DropdownMenuSubTrigger,
  DropdownMenuPortal,
  DropdownMenuSubContent,
  DropdownMenuItem,
} from "@/components/ui/dropdown-menu";
import { FC } from "react";

interface pageProps {
  name: string;
  nbAnnee: number;
}

const ProfileCard: FC<pageProps> = ({ name, nbAnnee }) => {
  return (
    <>
      <div className="flex flex-row align-middle justify-center">
        <Image
          src="https://media.licdn.com/dms/image/D4E03AQERtFsApnORRQ/profile-displayphoto-shrink_400_400/0/1666082759192?e=1694044800&v=beta&t=wbIhLijpWAZ8ozWkrHqUy2_PidkXEDn4dpV_N46iEPo"
          className="h-10 mr-3 rounded-full"
          alt="User Image"
          width={40}
          height={40}
        />
        <div className="flex flex-col">
          <p className="text-white">Bonjour, {name}</p>
          <p className="text-gray-200 text-xs">
            Élève en {nbAnnee}
            {nbAnnee == 1 ? "ère" : "ème"} année
          </p>
        </div>
        <div className="ml-10 place-self-center">
          <DropdownMenu>
            <DropdownMenuTrigger asChild>
              <svg
                className="w-4 h-4 text-gray-800 hover:cursor-pointer"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 8"
              >
                <path
                  stroke="currentColor"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="m1 1 5.326 5.7a.909.909 0 0 0 1.348 0L13 1"
                  color="white"
                />
              </svg>
            </DropdownMenuTrigger>
            <DropdownMenuContent className="w-56 bg-primary">
              <DropdownMenuSub>
                <DropdownMenuSubTrigger className="text-white hover:text-black focus:text-black focus-within:text-black pointer-events-none">
                  Change school
                </DropdownMenuSubTrigger>
                <DropdownMenuPortal className="hover:text-black text-white pointer-events-none">
                  <DropdownMenuSubContent className="bg-primary text-white">
                    <DropdownMenuItem>Email</DropdownMenuItem>
                    <DropdownMenuItem>Message</DropdownMenuItem>
                    <DropdownMenuSeparator />
                    <DropdownMenuItem>More...</DropdownMenuItem>
                  </DropdownMenuSubContent>
                </DropdownMenuPortal>
              </DropdownMenuSub>
              <DropdownMenuCheckboxItem className="text-white">
                Profile
              </DropdownMenuCheckboxItem>
              <DropdownMenuCheckboxItem className="text-white">
                Settings
              </DropdownMenuCheckboxItem>
              <DropdownMenuSeparator />
              <DropdownMenuCheckboxItem className="text-white">
                Logout
              </DropdownMenuCheckboxItem>
            </DropdownMenuContent>
          </DropdownMenu>
        </div>
      </div>
    </>
  );
};
export default ProfileCard;
